local voidtimer = 0

minetest.register_globalstep(function(dtime)
	voidtimer = voidtimer + dtime
	if voidtimer > 0.5 then
		voidtimer = 0
		local objs = minetest.object_refs
		for id, obj in pairs(objs) do
			local pos = obj:get_pos()
			if pos.y < -10 then
				minetest.set_node({ x = 1, y = 0, z = 0 }, { name = "default:stone" })
				minetest.set_node({ x = -1, y = 0, z = 0 }, { name = "default:stone" })
				minetest.set_node({ x = 1, y = 0, z = 1 }, { name = "default:stone" })
				minetest.set_node({ x = -1, y = 0, z = 1 }, { name = "default:stone" })
				minetest.set_node({ x = 1, y = 0, z = -1 }, { name = "default:stone" })
				minetest.set_node({ x = -1, y = 0, z = -1 }, { name = "default:stone" })
				minetest.set_node({ x = 0, y = 0, z = 1 }, { name = "default:stone" })
				minetest.set_node({ x = 0, y = 0, z = -1 }, { name = "default:stone" })
				minetest.set_node({ x = 0, y = 0, z = 0 }, { name = "default:meseblock" })
				obj:set_pos({ x = 0, y = 5, z = 0 })
			end
		end
	end
end)
