-- Stein
minetest.register_node("default:stone", {
	description = "Stein",
	tiles = {"default_stone.png"},
	is_ground_content = true,
	drop = "",
	groups = {dig_immediate=3}
})

-- Holz
minetest.register_node("default:wood", {
	description = "Holzbretter",
	paramtype2 = "facedir",
	place_param2 = 0,
	tiles = {"default_wood.png"},
	drop = "",
	groups = {dig_immediate=3}
})

-- Erde
minetest.register_node("default:dirt", {
	description = "Simple Erde",
	tiles = {"default_dirt.png"},
	drop = "",
	groups = {dig_immediate=3}
})

-- Erde mit Gras
minetest.register_node("default:dirt_with_grass", {
	description = "Erde mit Gras",
	tiles = {"default_grass.png", "default_dirt.png", {name = "default_dirt.png^default_grass_side.png", tileable_vertical = false}},
	drop = "",
	groups = {dig_immediate=3}
})

-- kaputte Truhe
minetest.register_node("default:nonworking_chest", {
	description = "kaputte Truhe",
	drop = "",
	groups = {dig_immediate=3},
	tiles = {
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png"
	},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.4375, -0.5, -0.4375, 0.4375, 0.125, 0.4375}, -- Unterseite
			{-0.4375, 0.125, -0.4375, 0.4375, 0.4375, 0.4375}, -- Oberseite
			{0.4375, 0, -0.0625, 0.5, 0.3125, 0.125}, -- Griff
		}
	},
})

-- Meseblock
minetest.register_node("default:meseblock", {
	description = 'Wertvolles St�ck "Gold"',
	tiles = {"default_mese.png"},
	light_source = 6,
	is_ground_content = false,
	drop = "",
	groups = {dig_immediate=3}
})