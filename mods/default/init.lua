local default_path = minetest.get_modpath("default")

dofile(default_path.."/commands.lua")
dofile(default_path.."/nodes.lua")
dofile(default_path.."/items.lua")
dofile(default_path.."/void.lua")
